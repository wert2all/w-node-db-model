'use strict';

/**
 * The entry point.
 *
 * @module DBModel
 */
module.exports = require('./src/ModelInterface');
module.exports.Factory = require('./src/Factory');
