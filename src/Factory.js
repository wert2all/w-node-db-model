'use strict';

class ModelFactory {
    /**
     *
     * @param {DBConnectionInterface} connection
     */
    constructor(connection) {
        /**
         *
         * @type {DBConnectionInterface}
         * @private
         */
        this._connection = connection;
    }

    /**
     *
     * @param {DBModelDefinitionInterface} modelDefinition
     * @param {{}} syncOptions
     * @return {Promise<DBModelInterface>}
     */
    create(modelDefinition, syncOptions = {}) {
        const model = this._connection.define(modelDefinition);
        return this._connection
            .sync(syncOptions)
            .then(() => model);
    }
}

module.exports = ModelFactory;
