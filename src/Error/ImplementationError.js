'use strict';

const ThrowableImplementationError = require('w-node-implementation-error');
/**
 * @class DBModelImplementationError
 * @type {DBModelImplementationError}
 * @extends {ThrowableImplementationError}
 */
module.exports = class DBModelImplementationError extends ThrowableImplementationError {

};
