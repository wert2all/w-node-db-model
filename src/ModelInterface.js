'use strict';
const DBModelImplementationError = require('./Error/ImplementationError');

/**
 * @class DBModelInterface
 * @type DBModelInterface
 * @interface
 */
class ModelInterface {
    /**
     *
     * @param {{}} data
     * @return {Promise<DBModelInterface>}
     * @abstract
     */
    // eslint-disable-next-line no-unused-vars
    save(data) {
        throw new DBModelImplementationError(this, 'save');
    }

    /**
     *
     * @param {{}} data
     * @return {DBModelInterface}
     * @abstract
     */
    // eslint-disable-next-line no-unused-vars
    create(data) {
        throw new DBModelImplementationError(this, 'build');
    }

    /**
     *
     * @param {DBModelInterface} child
     * @return {DBModelInterface}
     * @abstract
     */
    // eslint-disable-next-line no-unused-vars
    addChild(child) {
        throw new DBModelImplementationError(this, 'addChild');
    }

    /**
     * @return {string}
     * @abstract
     */
    getPrimaryKeyValue() {
        throw new DBModelImplementationError(this, 'addChild');
    }

    /**
     * @return {DBModelDefinitionInterface}
     * @abstract
     */
    getModelDefinition() {
        throw new DBModelImplementationError(this, 'getModelDefinition');
    }

    /**
     * @return {DBModelQueryInterface}
     * @abstract
     */
    getModelQuery() {
        throw new DBModelImplementationError(this, 'getModelQuery');
    }

    /**
     *
     * @param {DBModelQueryInterface} query
     * @return {Promise<EMEntity>}
     * @abstract
     */
    // eslint-disable-next-line no-unused-vars
    fetch(query) {
        throw new DBModelImplementationError(this, 'fetch');
    }
}

module.exports = ModelInterface;
module.exports.default = ModelInterface;
